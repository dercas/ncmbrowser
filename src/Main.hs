import System.Process.VM

import Control.Applicative
import Control.Concurrent
import Control.Monad
import Control.Monad.State
import Control.Monad.IO.Class

import Data.Either

import Foreign

import UI.NCurses
import UI.NCurses.Panel

data NEnv = NEnv
    { n_def :: Window
    , n_A   :: Window
    , n_B   :: Window
    , n_C   :: Window
    , n_pA  :: Panel
    , n_pB  :: Panel
    , n_pC  :: Panel
    , n_dim :: (Integer, Integer, Integer)
    }

type PosRef = (Double, Integer)
procPosRef :: Integer -> PosRef -> Integer
procPosRef i (r,a) = (i`mult`r) + a

data W = W 
    { w_win :: Window
    , w_pan :: Panel
    , w_yr  :: PosRef
    , w_xr  :: PosRef
    , w_hr  :: PosRef
    , w_wr  :: PosRef
    }

newW :: PosRef -> PosRef -> PosRef -> PosRef -> Curses W
newW hr wr yr xr = do
    (h',w') <- screenSize
    let fh = procPosRef h'
        fw = procPosRef w'
        y = fh yr
        x = fw xr
        h = fh hr
        w = fw wr
    win <- newWindow h w y x
    pan <- newPanel win
    return (W win pan yr xr hr wr)
    
resizeW :: Integer -> Integer -> W -> Curses ()
resizeW h' w' (W win p yr xr hr wr) = do
    hid <- panelHidden p
    let fh = procPosRef h'
        fw = procPosRef w'
        y = fh yr
        x = fw xr
        h = fh hr
        w = fw wr
    io $ print ((y,h,h'),(x,w,w'))
    -- when (not hid && y>=0 && y+h<=h' && x>=0 && x+w<=w') $ updateWindow win $ do
    let _y = if y<0 then 0 else y
        _x = if x<0 then 0 else x
        _h = if y+h>h' then h'-y else h
        _w = if x+w>w' then w'-x else w
    updateWindow win $ resizeWindow _h _w
    movePanel p _y _x
    return ()

hideW :: W -> Curses ()
hideW = hidePanel . w_pan

showW :: W -> Curses ()
showW = showPanel . w_pan
    
raiseW :: W -> Curses ()
raiseW = raisePanel . w_pan

lowerW :: W -> Curses ()
lowerW = lowerPanel . w_pan 

updateW :: W -> Update a -> Curses a
updateW w = updateWindow (w_win w)

type N = StateT NEnv Curses

runN go = runCurses $ do
    (h,w) <- screenSize
    let m = w `mult` 0.5
    def <- defaultWindow
    a <- newWindow h (m) 0 0
    b <- newWindow h (w-m-1) 0 (m+1)
    c <- newWindow 6 20 (h`mult`0.5-3) (w`mult`0.5-10)
    pa <- newPanel a
    pb <- newPanel b
    pc <- newPanel c
    hidePanel pc
    runStateT go $ NEnv def a b c pa pb pc (h,w,m)
    return ()

updateN :: N ()
updateN = do
    nevn@NEnv { n_def=def, n_A=a, n_B=b, n_C=c, n_pB=pb, n_pC=pc } <- get
    (h,w) <- lift $ screenSize
    let m = w  `mult` 0.5
    lift $ tryCurses $ do
        updateWindow def $ do
            clear
            moveCursor 0 m
            drawLineV Nothing h
        updateWindow a $ do
            clear
            resizeWindow h (m)
            drawBox Nothing Nothing
            drawString (show (m,w))
        updateWindow b $ do
            clear
            resizeWindow h (w-m-1)
            moveWindow 0 (m+1)
            drawBox Nothing Nothing
        updateWindow c $ do
            clear
            moveWindow (h`mult`0.5-3) (m-10)
            resizeWindow 6 20
            drawBox Nothing Nothing
        refreshPanels
        hidePanel pc
        refreshPanels
        hidePanel pc
        render
    put $ nevn { n_dim = (h,w,m) }

main = runCurses $ do
    setEcho False
    setCursorMode CursorInvisible
    def <- defaultWindow
    w <- newW (1,0) (0.5,0) (0,0) (0,0)
    w' <- newW (1,0) (0.5,1) (0,0) (0.5,0)
    w'' <- newW (0,6) (0,20) (0.5,-3) (0.5,-10)
    updateW w (drawBox Nothing Nothing)
    updateW w' (drawBox Nothing Nothing)
    updateW w'' (drawBox Nothing Nothing)
    refreshPanels
    render
    ret <- tryCurses $ forever $ do
        ev <- getEvent (w_win w) (Just 500)
        case ev of
            Nothing -> return ()
            Just (EventCharacter 'q') -> mzero
            Just EventResized -> do
                (hei,wid) <- screenSize
                resizeW hei wid w
                resizeW hei wid w'
                resizeW hei wid w''
                -- updateW w $ clear >> (drawBox Nothing Nothing)
                -- updateW w' $ clear >> (drawBox Nothing Nothing)
                -- updateW w'' $ clear >> (drawBox Nothing Nothing)
            Just (EventMouse _ st) -> do
                let (x,y,z) = mouseCoordinates st
                [a,b,c] <- sequence [enclosed win y x | win <- w_win <$> [w,w',w'']]
                updateW w $ do
                    -- clear
                    moveCursor 1 1
                    drawString (show (y,x,z))
                    moveCursor 2 1
                    drawString (show a)
                    moveCursor 3 1
                    drawString (show b)
                    moveCursor 4 1
                    drawString (show c)
                    drawBox Nothing Nothing
            _ -> return ()
        refreshPanels
        render
    either (io.print) (\_ -> return ()) ret
    io $ threadDelay 250000
    return ()

main' = runN $ do
    lift $ setCursorMode CursorInvisible
    lift $ setEcho False
    updateN
    p <- n_pC <$> get
    a <- n_A <$> get
    forever $ do
        ev <- lift $ getEvent a (Just 100)
        case ev of
            Just EventResized -> updateN >> (io $ print 1)
            _ -> return ()
        lift render
    io $ threadDelay 500000


mult :: Integral a => a -> Double -> a
mult n m = round $ (fromIntegral n)*m

io :: MonadIO m => IO a -> m a
io = liftIO
