{-# LANGUAGE OverloadedStrings, OverloadedLists, TypeFamilies #-}

import Data.ByteString.Internal
import Data.Char (ord)
import Data.List
import qualified Data.String as Str
import Data.Word

import Foreign
import Foreign.C

import qualified GHC.Exts as Exts
import GHC.ForeignPtr
import GHC.IO

import Numeric

import System.Process.VM


data Vec a = VEC (ForeignPtr Word8) Int

instance Show (Vec a) where
    show (VEC _ 0) = "( )"
    show (VEC ptr sz) = "(" ++ (unsafePerformIO $ withForeignPtr ptr p) ++ ")" where
        p :: Ptr Word8 -> IO String
        p ptr  = do
            xs <- peekArray sz ptr
            return $ concat $ intersperse " " $ p' <$> xs
        p' :: Word8 -> String
        p' word = do
            let str = showHex word ""
                l = length str
            (replicate (2-l) '0') ++ str

instance (Storable a) => Exts.IsList (Vec a) where
    type Item (Vec a) = a
    toList = toList
    fromList = fromList
    fromListN = fromListN

instance (Storable a) => Str.IsString (Vec a) where
    fromString = fromString 

fromString :: Storable a => String -> Vec a
fromString = fS undefined where
    fS :: Storable a => a -> String -> Vec a
    fS a str = unsafePerformIO $ do
        let n = length str
            sc = sizeOf (undefined :: Char)
            sz = sizeOf a
        ptr <- mallocPlainForeignPtrBytes (n*sz)
        withForeignPtr ptr $ \ptr' -> do
            fillBytes ptr' 0 (n*sz)
            pokeStr sz str ptr'
        return $ VEC ptr (n*sz)
    pokeStr :: Int -> String -> Ptr Word8 -> IO ()
    pokeStr sz str ptr = do
        if sz == 1 then do
            pokeArray (castPtr ptr) $ c8 <$> str
        else if sz == 2 then do
            pokeArray (castPtr ptr) $ c16 <$> str
        else if sz == 4 then do
            pokeArray (castPtr ptr) $ str
        else do
            pokeArr' sz ptr str
    pokeArr' :: Int -> Ptr a -> String -> IO ()
    pokeArr' sz ptr str = sequence_ . snd $
        mapAccumL (\n c -> (n+sz,pokeByteOff ptr n c)) 0 str
    c8  :: Char -> Word8
    c8 = fromIntegral . ord
    c16 :: Char -> Word16
    c16 = fromIntegral . ord
    c32 :: Char -> Word32
    c32 = fromIntegral . ord

fromList :: Storable a => [a] -> Vec a
fromList xs = fromListN (length xs) xs

fromListN :: Storable a => Int -> [a] -> Vec a
fromListN n xs = unsafePerformIO $ do
    let sz = n*(sizeOf $ head xs)
    ptr <- mallocPlainForeignPtrBytes sz
    withForeignPtr ptr $ \ptr -> pokeArray (castPtr ptr) xs
    return $ VEC ptr sz

toList :: Storable a => Vec a -> [a]
toList v@(VEC ptr sz) = unsafePerformIO $ withForeignPtr 
    (castForeignPtr ptr) $ peekArray (sz`quot`(sizeOf' v))

castVec :: Vec a -> Vec b
castVec (VEC ptr sz) = VEC ptr sz

eqVec :: Vec a -> Ptr b -> IO Bool
eqVec (VEC fptr sz) ptr = withForeignPtr fptr $ 
    \ptr' -> memeq (castPtr ptr) ptr' sz

searchVec :: Storable a => Vec a -> VM -> Word -> Word -> IO [Word]
searchVec vec vm add0 add1 = do
    if add0 > add1 then return []
    else do
        let sz = sizeOf' vec
        ptr <- readVM vm add0 sz
        eq <- eqVec vec ptr 
        xs <- searchVec vec vm (add0+fromIntegral sz) add1
        if eq then
            return (add0:xs)
        else
            return xs
    

-- aux

sizeOf' :: Storable a => m a -> Int
sizeOf' = sizeOf'' undefined where
    sizeOf'' :: Storable a => a -> m a -> Int
    sizeOf'' t _ = sizeOf t

memeq :: (Integral a) => Ptr Word8 -> Ptr Word8 -> a -> IO Bool
memeq pa pb sz = (==0) <$> c_memcmp pa pb (fromIntegral sz)

foreign import ccall unsafe "string.h memcmp"
    c_memcmp :: Ptr Word8 -> Ptr Word8 -> CSize -> IO CInt

